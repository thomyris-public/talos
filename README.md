[![npm version](https://img.shields.io/npm/v/talos.svg)](https://www.npmjs.com/package/ttalos)
[![downloads]](https://www.npmjs.com/package/ttalos)

[downloads]: https://img.shields.io/npm/dm/ttalos.svg
# TTalos

Libs for Fortalicia-cloud

## Summary
* [Decorators](#decorators)
  * [Profile](#profile)
  * [User session](#user-session)
* [Enums](#enums)
* [Guards](#guards)
  * [AuthGuardProfil](#authGuardProfil)
* [Scalars](#scalars)
  * [CodeScalar](#codescalar)
  * [CustomIdScalar](#customidscalar)
* [Options for API list](#options-for-api-list)


## Decorators

### Profile

### Implementation

```js
import { Profile, USER_PROFILE } from 'ttalos';
...
@UseGuards(AuthGuard)
@Profile(USER_PROFILE.THOMYRIS, USER_PROFILE.AGENCY, USER_PROFILE.PARTNER)
```

### User session

### Implementation

* Create NestJs decorator
```js
import { CurrentSessionDecorator, UserSession } from 'ttalos';

import { createParamDecorator, ExecutionContext } from '@nestjs/common';

import { TYPES } from '../../../config/injection/type';
import { Configuration } from '../../../config/env/configuration';
import { container } from '../../../config/injection/inversify.config';

export const CurrentSession = createParamDecorator(
  async (data: unknown, executionContext: ExecutionContext): Promise<UserSession> => {
    const currentSessionDecorator:CurrentSessionDecorator = new CurrentSessionDecorator(container.get<Configuration>(TYPES.config));
    return currentSessionDecorator.execute(data, executionContext);
  }
);
```

* Use
```js
async hello(@CurrentSession() session: UserSession): Promise<Boolean> {
  return 'hello';
}
```

## Guards

### AuthGuardProfil

```js
import { AuthGuardProfil } from 'ttalos';

import { Reflector } from '@nestjs/core';
import { CanActivate, ExecutionContext, Inject, Injectable } from '@nestjs/common';

import { TYPES } from '../../../../config/injection/type';
import { IsValidUsecase } from '../../../../usecase/auth/isValid.usecase';

@Injectable()
export class AuthGuard implements CanActivate {
  
  constructor(
    @Inject(TYPES.IsValidUsecase) private isValidUsecase: IsValidUsecase,
    private reflector: Reflector
  ) {}
  
  async canActivate(executionContext: ExecutionContext): Promise<boolean> {
    const authGuardProfil:AuthGuardProfil = new AuthGuardProfil(this.isValidUsecase, this.reflector);

    return authGuardProfil.execute(executionContext);
  }
}
```

* Use
```js
@UseGuards(AuthGuard)
@Profile(USER_PROFILE.THOMYRIS, USER_PROFILE.AGENCY, USER_PROFILE.PARTNER)
async hello(@CurrentSession() session: UserSession): Promise<Boolean> {
  return 'hello';
}
```

## Scalars

### AdresseScalar

```js
import { Adresse } from 'ttalos';
...
@Field(() => Adresse)
```

### CityScalar

```js
import { City } from 'ttalos';
...
@Field(() => City)
```

### CityScalar

```js
import { City } from 'ttalos';
...
@Field(() => City)
```

### CodeScalar

```js
import { Code } from 'ttalos';
...
@Field(() => Code)
```

### CustomIdScalar

```js
import { CustomId } from 'ttalos';
...
@Field(() => CustomId)
```

### DescriptionScalar

```js
import { Description } from 'ttalos';
...
@Field(() => Description)
```

### MailScalar

```js
import { Mail } from 'ttalos';
...
@Field(() => Mail)
```

### NameScalar

```js
import { Name } from 'ttalos';
...
@Field(() => Name)
```

### ObjectNameScalar

```js
import { ObjectName } from 'ttalos';
...
@Field(() => ObjectName)
```

### PasswordScalar

```js
import { Password } from 'ttalos';
...
@Field(() => Password)
```

### SerialNumberScalar

```js
import { SerialNumber } from 'ttalos';
...
@Field(() => SerialNumber)
```

### TokenScalar

```js
import { Token } from 'ttalos';
...
@Field(() => Token)
```

## Models & Dto

### IsValidRepositoryModel

```js
import { IsValidRepositoryModel, IsValidRepositoryDto } from 'ttalos';
...
isValid(dto: IsValidRepositoryDto): Promise<IsValidRepositoryModel>;
```

### IsValidUsecaseModel

```js
import { IsValidUsecaseModel, IsValidUsecaseDto } from 'ttalos';
...
async execute(input: IsValidUsecaseDto): Promise<IsValidUsecaseModel> {
...    
}
```

### UserSession

```js
import { UserSession } from 'ttalos';
...
async (data: unknown, executionContext: ExecutionContext): Promise<UserSession> => {
...    
}
```

### UserSessionUsecaseDto

```js
import { UserSessionUsecaseDto } from 'ttalos';
...
async execute(session: UserSessionUsecaseDto, input: ...): Promise<...> {
...    
}
```

### VersionUsecaseModel

```js
import { VersionUsecaseModel } from 'ttalos';
...
async execute(): Promise<VersionUsecaseModel> {
...    
}
```

## Logger

### NestLogger & logger

```js
import { NestLogger, Logger } from 'ttalos';
import { name } from '../package.json';

async function bootstrap() {
  Logger.defaultMeta.module = name;
  Logger.info('Environnement selected: ' + process.env.NODE_ENV);
  ...
  const app = await NestFactory.create(AppModule,{
    logger: new NestLogger(),
  });
  ...
  Logger.info('Server started on: ' + config.app.port);
}
```

## Mappers

### listMongoMapper

```js
import { ListMongoMapper } from 'ttalos';
...
const test = ListMongoMapper.fromBaseOptionToMongoFindOption(...);
...
```

### ListSqlMapper

```js
import { ListSqlMapper } from 'ttalos';
...
const test = ListSqlMapper.fromBaseOptionToSqlFindOption(...);
...
```

### ListSqlMapper

```js
import { SessionDecoratorMapper } from 'ttalos';
...
const test = SessionDecoratorMapper.sessionDtoFromJwt(...);
...
```


## Options for API list

```js
import {
  OptionBaseDto,
  OptionRepositoryDto,
  OptionUsecaseDto,
  OptionResolverDto,
  OptionFilterOperator,
  OptionSorterOperator,
} from 'ttalos';

const test = OptionFilterOperator.REGEX;

async list(@Args('input') dto?: OptionResolverDto): Promise<String[]> {
  return null;
}
```

## HOW TO publish

```
npm publish --access public
```