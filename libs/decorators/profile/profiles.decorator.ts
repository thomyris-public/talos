/* istanbul ignore file */
import { SetMetadata } from '@nestjs/common';

import { USER_PROFILE } from 'talos-commons';


export const PROFILE_KEY = 'profile';

export const Profile = (...roles: USER_PROFILE[]) => SetMetadata(PROFILE_KEY, roles);// eslint-disable-line @typescript-eslint/explicit-module-boundary-types
