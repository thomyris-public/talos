import { ERROR } from 'talos-commons';
import { JwtService } from '@nestjs/jwt';
import { ExtractJwt } from 'passport-jwt';
import { ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

import { Context } from '../../models/context.model';
import { UserSession } from '../../models/user.session.model';
import { SessionDecoratorMapper } from '../../mappers/userSesion/user.session.mapper';

export class CurrentSessionDecorator {
  constructor(
    private config: any,
  ) {}

  async execute(data: unknown, executionContext: ExecutionContext): Promise<UserSession> {
    const jwt = new JwtService();
    const ctx = GqlExecutionContext.create(executionContext);
    const context: Context = ctx.getContext();
    const accessToken = ExtractJwt.fromExtractors([ExtractJwt.fromAuthHeaderAsBearerToken()])(context.req);
    if (!accessToken) {
      throw new Error(ERROR.TOKEN_NOT_SET);
    }

    const jwtDecode = jwt.decode(accessToken);
    const session: UserSession = SessionDecoratorMapper.sessionDtoFromJwt(jwtDecode);
    return {
      id: session.id,
      code: session.code,
      profile: session.profile
    };
  }
}