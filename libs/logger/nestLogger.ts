import { LoggerService } from '@nestjs/common';

import { Logger } from './logger';

export class NestLogger implements LoggerService {
  log(message: string): void {
    Logger.info(message, {
      module: 'nestjs'
    });
  }
  error(message: string, trace: string): void { // eslint-disable-line @typescript-eslint/no-unused-vars
    // Handle by nestFormatterError in app module
  }
  warn(message: string): void {
    Logger.warn(message, {
      module: 'nestjs'
    });
  }
  debug(message: string): void {
    Logger.debug(message, {
      module: 'nestjs'
    });
  }
  verbose(message: string): void {
    Logger.verbose(message, {
      module: 'nestjs'
    });
  }
}
