import { createLogger, format, transports } from 'winston';

const myFormat = format.printf(info => {
  let myformat: string;
  if (info.error) {
    myformat = `${info.timestamp} ${info.module} ${info.level}: ${info.message} => ${info.error}`;
  } else {
    myformat = `${info.timestamp} ${info.module} ${info.level}: ${info.message}`;
  }
  return myformat;
});

// Default
const Logger = createLogger({
  level: 'debug',
  format: format.combine(
    format.errors({ stack: true }),
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    format.colorize(),
    myFormat
  ),
  defaultMeta: { module: null },
  transports: [
    new transports.Console()
  ],
});

export { Logger };
