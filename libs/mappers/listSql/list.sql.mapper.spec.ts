import { Op } from 'sequelize';
import { ListSqlMapper } from './list.sql.mapper';
import { OptionBaseDto } from '../../option/option.base.dto';
import { OptionFilterOperator, OptionSorterOperator } from '../../option/option.enum';


describe('UserDatasourceSqlMapper', () => {

  afterEach(()=>{
    jest.resetAllMocks();
    jest.clearAllMocks()
  })
  describe('#fromBaseOptionToSqlFindOption', () => {

    it('Should return empty option', () => {

      // Arrange
      // Act
      const result = ListSqlMapper.fromBaseOptionToSqlFindOption();

      // Expect
      expect(result).toStrictEqual(
        {
          filters: {},
          projection: null,
          options: null
        }
      );
    });

    it('Should return sort', () => {

      // Arrange
      const sorter: OptionBaseDto = {
        sorters: [
          {
            field: 'name',
            operator: OptionSorterOperator.ASC
          }
        ]
      };

      // Act
      const result = ListSqlMapper.fromBaseOptionToSqlFindOption(sorter);

      // Expect
      expect(result).toStrictEqual(
        {
          filters: {},
          projection: null,
          options: {
            sort: [['name', 'ASC']]
          }
        }
      );
    });

    it('Should return filter', () => {
      // Arrange
      // Act
      const result = ListSqlMapper.fromBaseOptionToSqlFindOption({
        filters: [
          {
            field: 'name',
            value: 'john'
          }
        ]
      });
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {
            name: 'john'
          },
          projection: null,
          options: null
        }
      );
    });

    it('Should return filter with regex', () => {
      // Arrange
      const expected = {
        filters: {
          name: { [Op.regexp]: '.*jhon.*' }
        },
        projection: null,
        options: null
      };
      // Act
      const result = ListSqlMapper.fromBaseOptionToSqlFindOption({
        filters: [
          {
            field: 'name',
            operator: OptionFilterOperator.REGEX,
            value: '.*jhon.*'
          }
        ]
      });
      // Expect
      expect(result).toStrictEqual(expected);
    });

    it('Should return pagination', () => {
      // Arrange
      // Act
      const result = ListSqlMapper.fromBaseOptionToSqlFindOption({
        pagination: {
          limit: 5,
          offset: 10
        }
      });
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {},
          projection: null,
          options: {
            limit: 5,
            skip: 10
          }
        }
      );
    });

    it('Should return pagination limit', () => {
      // Arrange
      // Act
      const result = ListSqlMapper.fromBaseOptionToSqlFindOption({
        pagination: {
          limit: 5
        }
      });
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {},
          projection: null,
          options: {
            limit: 5
          }
        }
      );
    });

    it('Should return pagination offset mongoose', () => {
      // Arrange
      // Act
      const result = ListSqlMapper.fromBaseOptionToSqlFindOption({
        pagination: {
          offset: 5
        }
      });
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {},
          projection: null,
          options: {
            skip: 5
          }
        }
      );
    });

  });

});