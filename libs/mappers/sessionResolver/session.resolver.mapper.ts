import { UserSession } from '../../models/user.session.model';
import { UserSessionUsecaseDto } from '../../dto/user.session.usecase.dto';

export class SessionResolverMapper {

  static sessionDtofromResolvertoUsecase(input: UserSession): UserSessionUsecaseDto {
    return {
      id: parseInt(input.id.toString()),
      code: input.code.toString(),
      profile: input.profile,
    };
  }

}