import { USER_PROFILE } from 'talos-commons';

import { UserSession } from '../../models/user.session.model';
import { SessionResolverMapper } from './session.resolver.mapper';
import { UserSessionUsecaseDto } from '../../dto/user.session.usecase.dto';


describe('session resolver mapper', () => {

  describe('sessionDtofromResolvertoUsecase', () => {

    it('mapper should return UserSessionUsecaseDto', () => {

      //Arrange
      const input : UserSession = {
        id: 1,
        code: 'pnom',
        profile: USER_PROFILE.CUSTOMER,
      };

      const expectedValue : UserSessionUsecaseDto = {
        id: 1,
        code: 'pnom',
        profile: USER_PROFILE.CUSTOMER,
      };

      //Act
      const res = SessionResolverMapper.sessionDtofromResolvertoUsecase(input);

      //Assert
      expect(res).toEqual(expectedValue);

    });

  });

});