import { USER_PROFILE } from 'talos-commons';
import { UserSession } from '../../models/user.session.model';
import { SessionDecoratorMapper } from './user.session.mapper';

describe('BoxResolver', () => {

  it('resolver should be defined', () => {

    // Arrange
    const input = {
      id: 1,
      code: 'thomyris',
      profile: USER_PROFILE.THOMYRIS,
    };

    const expectedRes: UserSession = {
      id: 1,
      code: 'thomyris',
      profile: USER_PROFILE.THOMYRIS,
    };

    // Act
    const res = SessionDecoratorMapper.sessionDtoFromJwt(input);

    // Assert
    expect(res).toStrictEqual(expectedRes);

  });

});