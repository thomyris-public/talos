import { UserSession } from '../../models/user.session.model';

export class SessionDecoratorMapper {

  static sessionDtoFromJwt(input: any): UserSession {

    return {
      id: parseInt(input.id.toString()),
      code: input.code.toString(),
      profile: input.profile,
    };

  }

}