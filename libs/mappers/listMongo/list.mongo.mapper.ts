import { OptionBaseDto } from '../../option/option.base.dto';
import { OptionFilterOperator, OptionSorterOperator } from '../../option/option.enum';

export class ListMongoMapper {

  static fromBaseOptionToMongoFindOption(dto?: OptionBaseDto): any { // eslint-disable-line @typescript-eslint/no-explicit-any

    const response:any = { // eslint-disable-line @typescript-eslint/no-explicit-any
      filters: {},
      projection: null,
      options: null
    }
    if ( dto ) {

      if ( dto.sorters ) {

        if (dto.sorters.length > 0) {
          const optionsSort:any = {}; // eslint-disable-line @typescript-eslint/no-explicit-any
          for (const elt of dto.sorters) {
            optionsSort[elt.field] = OptionSorterOperator[elt.operator].toLowerCase();
          }
          response.options = {
            sort: optionsSort
          }
        }

      }

      if ( dto.filters ) {

        if (dto.filters.length > 0) {

          const optionsFilter:any = {}; // eslint-disable-line @typescript-eslint/no-explicit-any
          for (const elt of dto.filters) {
            if (elt.operator === undefined || elt.operator === OptionFilterOperator.STRING) {
              optionsFilter[elt.field] = elt.value;
            } else {
              optionsFilter[elt.field] = { $regex: elt.value };
            }
          }
          response.filters = optionsFilter
        }

      }

      if ( dto.pagination ) {

        if (response.options === null) {
          response.options = {}
        }

        if (dto.pagination.limit) {
          response.options.limit = dto.pagination.limit;
        }

        if (dto.pagination.offset) {
          response.options.skip = dto.pagination.offset;
        }

      }

    }

    return response;
  }
}