import { ListMongoMapper } from './list.mongo.mapper';
import { OptionFilterOperator, OptionSorterOperator } from '../../option/option.enum';

describe('MqSourceBaseMongoMapper', () => {

  describe('#fromBaseOptionToMongoFindOption', () => {

    it('Should return option empty mongoose', () => {
      // Arrange
      // Act
      const result = ListMongoMapper.fromBaseOptionToMongoFindOption();
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {},
          projection: null,
          options: null
        }
      );
    });

    it('Should return sort mongoose', () => {
      // Arrange
      // Act
      const result = ListMongoMapper.fromBaseOptionToMongoFindOption({
        sorters: [
          {
            field: 'name',
            operator: OptionSorterOperator.ASC
          }
        ]
      });
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {},
          projection: null,
          options: {
            sort: {
              name: 'asc'
            }
          }
        }
      );
    });

    it('Should return filter mongoose', () => {
      // Arrange
      // Act
      const result = ListMongoMapper.fromBaseOptionToMongoFindOption({
        filters: [
          {
            field: 'name',
            value: 'john'
          }
        ]
      });
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {
            name: 'john'
          },
          projection: null,
          options: null
        }
      );
    });

    it('Should return filter with regex mongoose', () => {
      // Arrange
      // Act
      const result = ListMongoMapper.fromBaseOptionToMongoFindOption({
        filters: [
          {
            field: 'name',
            operator: OptionFilterOperator.REGEX,
            value: '.*jhon.*'
          }
        ]
      });
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {
            name: { $regex: '.*jhon.*' }
          },
          projection: null,
          options: null
        }
      );
    });

    it('Should return pagination mongoose', () => {
      // Arrange
      // Act
      const result = ListMongoMapper.fromBaseOptionToMongoFindOption({
        pagination: {
          limit: 5,
          offset: 10
        }
      });
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {},
          projection: null,
          options: {
            limit: 5,
            skip: 10
          }
        }
      );
    });

    it('Should return pagination limit mongoose', () => {
      // Arrange
      // Act
      const result = ListMongoMapper.fromBaseOptionToMongoFindOption({
        pagination: {
          limit: 5
        }
      });
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {},
          projection: null,
          options: {
            limit: 5
          }
        }
      );
    });

    it('Should return pagination offset mongoose', () => {
      // Arrange
      // Act
      const result = ListMongoMapper.fromBaseOptionToMongoFindOption({
        pagination: {
          offset: 5
        }
      });
      // Expect
      expect(result).toStrictEqual(
        {
          filters: {},
          projection: null,
          options: {
            skip: 5
          }
        }
      );
    });

  });


});