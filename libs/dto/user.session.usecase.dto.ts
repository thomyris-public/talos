import { USER_PROFILE } from 'talos-commons';

export interface UserSessionUsecaseDto {
  id: number;
  code: string;
  profile: USER_PROFILE;
}