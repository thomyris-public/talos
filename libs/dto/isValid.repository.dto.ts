export class IsValidRepositoryDto {
  token: string;
  profiles: string[];
}