import { USER_PROFILE } from 'talos-commons';

import { Code } from '../scalars/code/code.scalar';

export interface UserSession {
  id: number;
  code: Code;
  profile: USER_PROFILE;
}