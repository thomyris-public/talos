import { OptionFilterOperator, OptionSorterOperator } from './option.enum';

export class OptionUsecaseDto {
  filters?: OptionFilterUsecaseDto[]
  pagination?: OptionPaginationUsecaseDto
  sorters?: OptionSorterUsecaseDto[]
}

export class OptionFilterUsecaseDto {
  field: string;
  operator?: OptionFilterOperator;
  value: string;
}

export class OptionPaginationUsecaseDto {
  limit?: number;
  offset?: number;
}

export class OptionSorterUsecaseDto {
  field: string;
  operator: OptionSorterOperator;
}