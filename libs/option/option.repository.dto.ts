import { OptionFilterOperator, OptionSorterOperator } from './option.enum';

export class OptionRepositoryDto {
  filters?: OptionFilterRepositoryDto[]
  pagination?: OptionPaginationRepositoryDto
  sorters?: OptionSorterRepositoryDto[]
}

export class OptionFilterRepositoryDto {
  field: string;
  operator?: OptionFilterOperator;
  value: string;
}

export class OptionPaginationRepositoryDto {
  limit?: number;
  offset?: number;
}

export class OptionSorterRepositoryDto {
  field: string;
  operator: OptionSorterOperator;
}