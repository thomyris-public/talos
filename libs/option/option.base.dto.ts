import { OptionFilterOperator, OptionSorterOperator } from './option.enum';

export class OptionBaseDto {
  filters?: OptionFilterBaseDto[]
  pagination?: OptionPaginationBaseDto
  sorters?: OptionSorterBaseDto[]
}

export class OptionFilterBaseDto {
  field: string;
  operator?: OptionFilterOperator;
  value: string;
}

export class OptionPaginationBaseDto {
  limit?: number;
  offset?: number;
}

export class OptionSorterBaseDto {
  field: string;
  operator: OptionSorterOperator;
}