import { Field, InputType, registerEnumType } from '@nestjs/graphql';

import { OptionFilterOperator, OptionSorterOperator } from './option.enum';

registerEnumType(OptionFilterOperator, {
  name: 'OptionFilterOperator',
});

registerEnumType(OptionSorterOperator, {
  name: 'OptionSorterOperator',
});

@InputType()
export class OptionFilterResolverDto {
  @Field(() => String, { nullable: false, description: 'Field name' })
    field: string;

  @Field(() => OptionFilterOperator, { nullable: true, defaultValue: OptionFilterOperator.STRING, description: 'Field operator STRING or REGEX' })
    operator?: OptionFilterOperator;

  @Field(() => String, { nullable: false, description: 'Field value' })
    value: string;
}

@InputType()
export class OptionPaginationResolverDto {
  @Field(() => Number, { nullable: true, description: 'Field limit' })
    limit?: number;

  @Field(() => Number, { nullable: true, description: 'Field offset' })
    offset?: number;
}

@InputType()
export class OptionSorterResolverDto {
  @Field(() => String, { nullable: false, description: 'Field name' })
    field: string;

  @Field(() => OptionSorterOperator, { nullable: false, description: 'Field operator ASC, DESC' })
    operator: OptionSorterOperator;
}

@InputType()
export class OptionResolverDto {

  @Field(() => [OptionFilterResolverDto], { nullable: true, description: 'List of filter' })
    filters?: OptionFilterResolverDto[]

  @Field(() => OptionPaginationResolverDto, { nullable: true, description: 'Pagination option' })
    pagination?: OptionPaginationResolverDto

  @Field(() => [OptionSorterResolverDto], { nullable: true, description: 'List of sorter' })
    sorters?: OptionSorterResolverDto[]
}