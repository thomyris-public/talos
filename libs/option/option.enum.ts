export enum OptionFilterOperator {
  REGEX,
  STRING,
}

export enum OptionSorterOperator {
  ASC,
  DESC,
}