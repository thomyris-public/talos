import { ERROR } from 'talos-commons';
import { Reflector } from '@nestjs/core';
import { ExtractJwt } from 'passport-jwt';
import { ExecutionContext } from '@nestjs/common';
import { mock, MockProxy } from 'jest-mock-extended';
import { GqlExecutionContext } from '@nestjs/graphql';

import { AuthGuardProfil } from './authProfile.guard';

describe('AuthGuardProfil', ()=>{

  let authGuardProfil: AuthGuardProfil;
  const mockExecutionContext: MockProxy<ExecutionContext> = mock<ExecutionContext>();
  const mockIsValidUsecase: MockProxy<any> = mock<any>();
  const mockReflector: MockProxy<Reflector> = mock<Reflector>();

  const tContext = {
    getContext: () => {
      return {
        req: {
          user: 'thomyris'
        },
        res: {
          header: ''
        }
      }
    }
  };

  beforeAll(async () => {
    authGuardProfil = new AuthGuardProfil(mockIsValidUsecase, mockReflector);
    GqlExecutionContext.create = jest.fn().mockImplementation(() => tContext);
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(authGuardProfil).toBeDefined();
  });

  describe('canActivate', ()=>{

    it('Should be defined', async () => {
      expect(authGuardProfil.execute).toBeDefined();
    });

    it('Should return boolean', async () => {

      // Arrange
      const isValidExpected = {
        response: true,
        errorCode: null,
      };

      ExtractJwt.fromAuthHeaderAsBearerToken = jest.fn().mockImplementation(() => 'sdfkjsdfkljsldkjf');
      ExtractJwt.fromExtractors = jest.fn().mockImplementation(() => () => {
        return 'token';
      });

      mockReflector.get.mockReturnValue(['THOMYRIS'])
      mockIsValidUsecase.execute.mockResolvedValue(isValidExpected);

      // Act
      const res = await authGuardProfil.execute(mockExecutionContext);

      // Assert
      expect(res).toStrictEqual(true);

    });

    it('Should return true (without requiredRoles) ', async () => {

      // Arrange
      ExtractJwt.fromAuthHeaderAsBearerToken = jest.fn().mockImplementation(() => 'sdfkjsdfkljsldkjf');
      ExtractJwt.fromExtractors = jest.fn().mockImplementation(() => () => {
        return 'token';
      });
      mockReflector.get.mockReturnValue(null);

      // Act
      const res = await authGuardProfil.execute(mockExecutionContext);

      // Assert
      expect(res).toStrictEqual(true);

    });

    it('Should return true (with isValid) ', async () => {

      // Arrange
      ExtractJwt.fromAuthHeaderAsBearerToken = jest.fn().mockImplementation(() => 'sdfkjsdfkljsldkjf');
      ExtractJwt.fromExtractors = jest.fn().mockImplementation(() => () => {
        return 'token';
      });

      mockReflector.get.mockReturnValue(['THOMYRIS']);
      mockIsValidUsecase.execute.mockResolvedValue({
        response: true,
        errorCode: null,
      });

      // Act
      const res = await authGuardProfil.execute(mockExecutionContext);

      // Assert
      expect(res).toStrictEqual(true);

    });

    it('Should return error TOKEN_NOT_SET', async () => {

      // Arrange
      const isValidExpected = {
        response: true,
        errorCode: null,
      };

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ExtractJwt.fromAuthHeaderAsBearerToken = jest.fn().mockImplementation(():any => null);
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ExtractJwt.fromExtractors = jest.fn().mockImplementation(():any => ():any => {
        return null;
      });

      mockReflector.get.mockReturnValue(['THOMYRIS']);
      mockIsValidUsecase.execute.mockResolvedValue(isValidExpected);

      // Act
      let error;
      try {
        await authGuardProfil.execute(mockExecutionContext);
      } catch (e) {
        error = e;
      }

      // Assert
      expect(error).toStrictEqual(new Error(ERROR.TOKEN_NOT_SET));

    });

    it('Should return error INVALID_TOKEN', async () => {

      // Arrange
      const isValidExpected = {
        response: false,
        errorCode: ERROR.INVALID_TOKEN,
      };

      ExtractJwt.fromAuthHeaderAsBearerToken = jest.fn().mockImplementation(() => 'sdfkjsdfkljsldkjf');
      ExtractJwt.fromExtractors = jest.fn().mockImplementation(() => () => {
        return 'token';
      });

      mockReflector.get.mockReturnValue(['THOMYRIS']);
      mockIsValidUsecase.execute.mockResolvedValue(isValidExpected);

      // Act
      let error;
      try {
        await authGuardProfil.execute(mockExecutionContext);
      } catch (e) {
        error = e;
      }

      // Assert
      expect(error).toStrictEqual(new Error(ERROR.INVALID_TOKEN));

    });

  });

});