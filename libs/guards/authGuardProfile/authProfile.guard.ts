import { Reflector } from '@nestjs/core';
import { ExtractJwt } from 'passport-jwt';
import { ExecutionContext } from '@nestjs/common';
import { ERROR, USER_PROFILE } from 'talos-commons';
import { GqlExecutionContext } from '@nestjs/graphql';

import { Context } from '../../models/context.model';
import { PROFILE_KEY } from '../../decorators/profile/profiles.decorator';

export class AuthGuardProfil {

  constructor(
    private isValidUsecase: any,
    private reflector: Reflector,
  ) {}

  async execute(executionContext: ExecutionContext): Promise<boolean> {

    const gqlExecutionContext = GqlExecutionContext.create(executionContext);
    const context: Context = gqlExecutionContext.getContext();
    const accessToken = ExtractJwt.fromExtractors([ExtractJwt.fromAuthHeaderAsBearerToken()])(context.req);
    if (!accessToken) {
      throw new Error(ERROR.TOKEN_NOT_SET);
    }

    const requiredRoles = this.reflector.get<USER_PROFILE[]>(PROFILE_KEY, executionContext.getHandler());
    if (!requiredRoles) {
      return true;
    }

    const isValidDto = {
      token: accessToken,
      profiles: requiredRoles
    };

    const res = await this.isValidUsecase.execute(isValidDto);

    if (!res.response){
      throw new Error(res.errorCode)
    }
    return res.response;
  }
}