import { Kind, ValueNode } from 'graphql';
import { ERROR, REGEX } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';


export class City extends String {}

@Scalar('City', /* istanbul ignore next */ () => City)
export class CityScalar implements CustomScalar<string, City> {

  description = 'City scalar type';

  private check(value: string): City {
    const regExp = new RegExp(REGEX.CITY);
    if (regExp.test(value)) {
      return value as City;
    }
    throw new Error(ERROR.INVALID_CITY);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): City {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): City {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_CITY);
  }
}
