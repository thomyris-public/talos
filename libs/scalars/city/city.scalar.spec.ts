import { Kind, ValueNode } from 'graphql';
import { CityScalar } from './city.scalar';


describe('CityScalar', () => {

  const scalar = new CityScalar();

  describe('#parseValue', () => {

    it('Should return a valid City', () => {

      // Act
      const result = scalar.parseValue('dnsZZ');

      // Assert
      expect(result).toStrictEqual('dnsZZ');

    });

    it('Should throw an INVALID_CITY error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('to$')).toThrow('INVALID_CITY');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the city', () => {

      // Act
      const result = scalar.serialize('dns1ZZ');

      // Assert
      expect(result).toStrictEqual('dns1ZZ');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid City', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'dnsZZ',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('dnsZZ');

    });

    it('Should throw an INVALID_CITY error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'toto#',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.INT,
        value: '12',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow(
        'INVALID_CITY',
      );
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_CITY',
      );

    });

  });

});
