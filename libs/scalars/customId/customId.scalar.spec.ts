import { Kind, ValueNode } from 'graphql';

import { CustomIdScalar } from './customId.scalar';

describe('CustomIDScalar', () => {

  const scalar = new CustomIdScalar();

  describe('#parseValue', () => {

    it('Should return a valid ObjectID', () => {

      // Act
      const result = scalar.parseValue(1);

      // Assert
      expect(result).toStrictEqual(1);

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the ObjectID', () => {

      // Act
      const result = scalar.serialize(1);

      // Assert
      expect(result).toStrictEqual(1);

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid ObjectID', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: '62fcb0d5ce9350c249a9e23a',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('62fcb0d5ce9350c249a9e23a');

    });

    it('Should throw an INVALID_OBJECT_ID error', () => {

      // Arrange
      const tValueNode2: ValueNode = {
        kind: Kind.STRING,
        value: '1',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_OBJECT_ID',
      );

    });

    it('Should throw an INVALID_OBJECT_ID error', () => {

      // Arrange
      const tValueNode2: ValueNode = {
        kind: Kind.INT,
        value: '1',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_OBJECT_ID',
      );

    });

  });

});
