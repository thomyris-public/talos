import { ObjectId } from 'mongodb';
import { ERROR } from 'talos-commons';
import { Kind, ValueNode } from 'graphql';
import { CustomScalar, Scalar } from '@nestjs/graphql';


export class CustomId extends String {}

@Scalar('CustomId', /* istanbul ignore next */ () => CustomId)
export class CustomIdScalar implements CustomScalar<string, CustomId> {

  description = 'CustomId scalar type';

  private checkID(value: string): CustomId {
    if (ObjectId.isValid(value)) {
      return value as CustomId;
    }
    throw new Error(ERROR.INVALID_OBJECT_ID);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): CustomId {
    return this.checkID(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): CustomId {
    if (ast.kind === Kind.STRING) {
      return this.checkID(ast.value);
    }
    throw new Error(ERROR.INVALID_OBJECT_ID);
  }
}
