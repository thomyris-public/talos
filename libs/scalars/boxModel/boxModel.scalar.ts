import { Kind, ValueNode } from 'graphql';

import { ERROR } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';


export class BoxModel extends String {}

@Scalar('BoxModel', /* istanbul ignore next */ () => BoxModel)
export class BoxModelScalar implements CustomScalar<string, BoxModel> {

  description = 'boxModel scalar type';
  models= ['FTA15-A1', 'FTA15-A2', 'FTA15-B1']
  private check(value: string): BoxModel {
    if (this.models.includes(value)) {
      return value as BoxModel;
    }
    throw new Error(ERROR.BOX_MODEL_NOT_EXIST);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): BoxModel {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): BoxModel {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.BOX_MODEL_NOT_EXIST);
  }
}
