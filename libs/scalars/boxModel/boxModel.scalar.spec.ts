import { Kind, ValueNode } from 'graphql';
import { BoxModelScalar } from './boxModel.scalar';


describe('BoxModelScalar', () => {

  const scalar = new BoxModelScalar();

  describe('#parseValue', () => {

    it('Should return a valid boxmodel', () => {

      // Act
      const result = scalar.parseValue('FTA15-A1');

      // Assert
      expect(result).toStrictEqual('FTA15-A1');

    });

    it('Should throw an BOX_MODEL_NOT_EXIST error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('FTA15')).toThrow('BOX_MODEL_NOT_EXIST');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing boxmodel', () => {

      // Act
      const result = scalar.serialize('FTA15-A1');

      // Assert
      expect(result).toStrictEqual('FTA15-A1');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid boxmodel', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'FTA15-A1',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('FTA15-A1');

    });

    it('Should throw an BOX_MODEL_NOT_EXIST error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.INT,
        value: 'A1',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.INT,
        value: 'FTA15',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow(
        'BOX_MODEL_NOT_EXIST',
      );
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'BOX_MODEL_NOT_EXIST',
      );

    });

  });

});
