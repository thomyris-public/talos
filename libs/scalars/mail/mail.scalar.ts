import { Kind, ValueNode } from 'graphql';
import { ERROR, REGEX } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';

export class Mail extends String {}

@Scalar('Mail', /* istanbul ignore next */ () => Mail)
export class MailScalar implements CustomScalar<string, Mail> {

  description = 'Mail scalar type';

  private check(value: string): Mail {
    const regExp = new RegExp(REGEX.USER_MAIL);
    if (regExp.test(value)) {
      return value as Mail;
    }
    throw new Error(ERROR.INVALID_MAIL);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): Mail {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): Mail {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_MAIL);
  }
}
