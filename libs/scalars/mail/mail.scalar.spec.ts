import { Kind, ValueNode } from 'graphql';

import { MailScalar } from './mail.scalar';

describe('MailScalar', () => {

  const scalar = new MailScalar();

  describe('#parseValue', () => {

    it('Should return a valid Mail', () => {

      // Act
      const result = scalar.parseValue('loicdamiano@gmail.com');

      // Assert
      expect(result).toStrictEqual('loicdamiano@gmail.com');

    });

    it('Should throw an INVALID_MAIL error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('test')).toThrow('INVALID_MAIL');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the Mail', () => {

      // Act
      const result = scalar.serialize('loicdamiano@gmail.com');

      // Assert
      expect(result).toStrictEqual('loicdamiano@gmail.com');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid Mail', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'loicdamiano@gmail.com',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('loicdamiano@gmail.com');

    });

    it('Should throw an INVALID_MAIL error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'loicdamgmail.com#',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.INT,
        value: '12',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow(
        'INVALID_MAIL',
      );
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_MAIL',
      );

    });

  });

});
