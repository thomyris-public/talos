import { Kind, ValueNode } from 'graphql';
import { ObjectNameScalar } from './objectName.scalar';


describe('ObjectNameScalar', () => {

  const scalar = new ObjectNameScalar();

  describe('#parseValue', () => {

    it('Should return a valid ObjectName', () => {

      // Act
      const result = scalar.parseValue('dnsZZ');

      // Assert
      expect(result).toStrictEqual('dnsZZ');

    });

    it('Should throw an INVALID_OBJECT_NAME error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('to$')).toThrow('INVALID_OBJECT_NAME');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the ObjectName', () => {

      // Act
      const result = scalar.serialize('dns1ZZ');

      // Assert
      expect(result).toStrictEqual('dns1ZZ');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid ObjectName', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'dnsZZ',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('dnsZZ');

    });

    it('Should throw an INVALID_OBJECT_NAME error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'toto#',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.INT,
        value: '12',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow(
        'INVALID_OBJECT_NAME',
      );
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_OBJECT_NAME',
      );

    });

  });

});
