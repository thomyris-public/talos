import { Kind, ValueNode } from 'graphql';
import { ERROR, REGEX } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';


export class ObjectName extends String {}

@Scalar('ObjectName', /* istanbul ignore next */ () => ObjectName)
export class ObjectNameScalar implements CustomScalar<string, ObjectName> {

  description = 'ObjectName scalar type';

  private check(value: string): ObjectName {
    const regExp = new RegExp(REGEX.OBJECT_NAME);
    if (regExp.test(value)) {
      return value as ObjectName;
    }
    throw new Error(ERROR.INVALID_OBJECT_NAME);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): ObjectName {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): ObjectName {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_OBJECT_NAME);
  }
}
