import { Kind, ValueNode } from 'graphql';

import { TokenScalar } from './token.scalar';

describe('TokenScalar', () => {

  const scalar = new TokenScalar();

  describe('#parseValue', () => {

    it('Should return a valid token', () => {

      // Act
      const result = scalar.parseValue('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb2RlIjoibGRhbWkiLCJpYXQiOjE2NTI3MDMxNzcsImV4cCI6MTY1MjcwNjE3N30.FJFZpNvA306K3iS-L7-3wTaD5Zonwa7zKUrJAAMg5oA');

      // Assert
      expect(result).toStrictEqual('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb2RlIjoibGRhbWkiLCJpYXQiOjE2NTI3MDMxNzcsImV4cCI6MTY1MjcwNjE3N30.FJFZpNvA306K3iS-L7-3wTaD5Zonwa7zKUrJAAMg5oA');

    });

    it('Should throw an INVALID_TOKEN error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('4444444')).toThrow('INVALID_TOKEN');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the ObjectID', () => {

      // Act
      const result = scalar.serialize('ldami');

      // Assert
      expect(result).toStrictEqual('ldami');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid ObjectID', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb2RlIjoibGRhbWkiLCJpYXQiOjE2NTI3MDMxNzcsImV4cCI6MTY1MjcwNjE3N30.FJFZpNvA306K3iS-L7-3wTaD5Zonwa7zKUrJAAMg5oA',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb2RlIjoibGRhbWkiLCJpYXQiOjE2NTI3MDMxNzcsImV4cCI6MTY1MjcwNjE3N30.FJFZpNvA306K3iS-L7-3wTaD5Zonwa7zKUrJAAMg5oA');

    });

    it('Should throw an INVALID_TOKEN error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.INT,
        value: '1',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.STRING,
        value: '44444',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow(
        'INVALID_TOKEN',
      );
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_TOKEN',
      );

    });

  });

});
