import { Kind, ValueNode } from 'graphql';
import { ERROR, REGEX } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';

export class Token extends String {}

@Scalar('Token', /* istanbul ignore next */ () => Token)
export class TokenScalar implements CustomScalar<string, Token> {

  description = 'TokenScalar scalar type';

  private check(value: string): Token {
    const regExp = new RegExp(REGEX.TOKEN_FORMAT);
    if (regExp.test(value)) {
      return value as Token;
    }
    throw new Error(ERROR.INVALID_TOKEN);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): Token {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): Token {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_TOKEN);
  }
}
