import { Kind, ValueNode } from 'graphql';
import { AdresseScalar } from './adresse.scalar';


describe('AdresseScalar', () => {

  const scalar = new AdresseScalar();

  describe('#parseValue', () => {

    it('Should return a valid Adresse', () => {

      // Act
      const result = scalar.parseValue('dnsZZ');

      // Assert
      expect(result).toStrictEqual('dnsZZ');

    });

    it('Should throw an INVALID_ADRESSE error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('to$')).toThrow('INVALID_ADRESSE');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the Adresse', () => {

      // Act
      const result = scalar.serialize('dns1ZZ');

      // Assert
      expect(result).toStrictEqual('dns1ZZ');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid Adresse', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'dnsZZ',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('dnsZZ');

    });

    it('Should throw an INVALID_ADRESSE error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'toto#',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.INT,
        value: '12',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow(
        'INVALID_ADRESSE',
      );
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_ADRESSE',
      );

    });

  });

});