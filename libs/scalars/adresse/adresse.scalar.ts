import { Kind, ValueNode } from 'graphql';
import { ERROR, REGEX } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';


export class Adresse extends String {}

@Scalar('Adresse', /* istanbul ignore next */ () => Adresse)
export class AdresseScalar implements CustomScalar<string, Adresse> {

  description = 'Adresse scalar type';

  private check(value: string): Adresse {
    const regExp = new RegExp(REGEX.ADRESS_LOCATION);
    if (regExp.test(value)) {
      return value as Adresse;
    }
    throw new Error(ERROR.INVALID_ADRESSE);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): Adresse {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): Adresse {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_ADRESSE);
  }
}
