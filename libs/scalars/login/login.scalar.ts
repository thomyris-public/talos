import { Kind, ValueNode } from 'graphql';
import { REGEX, ERROR } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';

export class Login extends String {}

@Scalar('Login', /* istanbul ignore next */ () => Login)
export class LoginScalar implements CustomScalar<string, Login> {

  description = 'Login scalar type';

  private check(value: string): Login {
    const regExp = new RegExp(REGEX.USER_LOGIN);
    if (regExp.test(value)) {
      return value as Login;
    }
    throw new Error(ERROR.INVALID_LOGIN_FIELD);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): Login {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): Login {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_LOGIN_FIELD);
  }
}
