import { Kind, ValueNode } from 'graphql';

import { NameScalar } from './name.scalar';

describe('NameScalar', () => {

  const scalar = new NameScalar();

  describe('#parseValue', () => {

    it('Should return a valid Name', () => {

      // Act
      const result = scalar.parseValue('dnsZZ');

      // Assert
      expect(result).toStrictEqual('dnsZZ');

    });

    it('Should throw an INVALID_NAME error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('to$')).toThrow('INVALID_NAME');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the Name', () => {

      // Act
      const result = scalar.serialize('dns1ZZ');

      // Assert
      expect(result).toStrictEqual('dns1ZZ');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid Name', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'dnsZZ',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('dnsZZ');

    });

    it('Should throw an INVALID_NAME error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'toto#',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.INT,
        value: '12',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow(
        'INVALID_NAME',
      );
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_NAME',
      );

    });

  });

});
