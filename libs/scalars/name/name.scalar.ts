import { Kind, ValueNode } from 'graphql';
import { ERROR, REGEX } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';


export class Name extends String {}

@Scalar('Name', /* istanbul ignore next */ () => Name)
export class NameScalar implements CustomScalar<string, Name> {

  description = 'Name scalar type';

  private check(value: string): Name {
    // const regExp = new RegExp(REGEX.NAME);
    const regExp = new RegExp(REGEX.USER_NAME);
    if (regExp.test(value)) {
      return value as Name;
    }
    throw new Error(ERROR.INVALID_NAME);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): Name {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): Name {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_NAME);
  }
}
