import { Kind, ValueNode } from 'graphql';

import { CodeScalar } from './code.scalar';

describe('CodeScalar', () => {

  const scalar = new CodeScalar();

  describe('#parseValue', () => {

    it('Should return a valid ObjectID', () => {

      // Act
      const result = scalar.parseValue('ldami');

      // Assert
      expect(result).toStrictEqual('ldami');

    });

    it('Should throw an INVALID_CODE error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('4444444')).toThrow('INVALID_CODE');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the ObjectID', () => {

      // Act
      const result = scalar.serialize('ldami');

      // Assert
      expect(result).toStrictEqual('ldami');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid ObjectID', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'ldami',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('ldami');

    });

    it('Should throw an INVALID_CODE error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.INT,
        value: '1',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.STRING,
        value: '44444',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow(
        'INVALID_CODE',
      );
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_CODE',
      );

    });

  });

});
