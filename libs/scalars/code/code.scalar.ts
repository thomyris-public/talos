import { Kind, ValueNode } from 'graphql';
import { REGEX, ERROR } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';

export class Code extends String {}

@Scalar('Code', /* istanbul ignore next */ () => Code)
export class CodeScalar implements CustomScalar<string, Code> {

  description = 'Code scalar type';

  private check(value: string): Code {
    const regExp = new RegExp(REGEX.USER_CODE);
    if (regExp.test(value)) {
      return value as Code;
    }
    throw new Error(ERROR.INVALID_CODE);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): Code {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): Code {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_CODE);
  }
}
