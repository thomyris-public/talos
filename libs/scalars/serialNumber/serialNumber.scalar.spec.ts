import { Kind, ValueNode } from 'graphql';
import { SerialNumberScalar } from './serialNumber.scalar';

describe('SerialNumberScalar', () => {

  const scalar = new SerialNumberScalar();

  describe('#parseValue', () => {

    it('Should return a valid ObjectID', () => {

      // Act
      const result = scalar.parseValue('0800.278B.9730');

      // Assert
      expect(result).toStrictEqual('0800.278B.9730');

    });

    it('Should throw an INVALID_CODE error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('4')).toThrow('INVALID_SERIAL_NUMBER');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the serial_numnber', () => {

      // Act
      const result = scalar.serialize('0800.278B.9730');

      // Assert
      expect(result).toStrictEqual('0800.278B.9730');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid serial_numnber', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: '0800.278B.9730',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('0800.278B.9730');

    });

    it('Should throw an INVALID_SERIAL_NUMBER error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.INT,
        value: '0800.278B.9730',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.INT,
        value: '44444',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow(
        'INVALID_SERIAL_NUMBER',
      );
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_SERIAL_NUMBER',
      );

    });

  });

});
