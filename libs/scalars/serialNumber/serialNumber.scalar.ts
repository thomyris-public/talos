import { Kind, ValueNode } from 'graphql';
import { ERROR, REGEX } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';


export class SerialNumber extends String {}

@Scalar('SerialNumber', /* istanbul ignore next */ () => SerialNumber)
export class SerialNumberScalar implements CustomScalar<string, SerialNumber> {

  description = 'SerialNumber scalar type';

  private check(value: string): SerialNumber {
    const regExp = new RegExp(REGEX.FORTALICIA_SERIAL_NUMBER);
    if (regExp.test(value)) {
      return value as SerialNumber;
    }
    throw new Error(ERROR.INVALID_SERIAL_NUMBER);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): SerialNumber {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): SerialNumber {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_SERIAL_NUMBER);
  }
}
