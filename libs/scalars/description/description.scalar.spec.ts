import { Kind, ValueNode } from 'graphql';
import { DescriptionScalar } from './description.scalar';


describe('DescriptionScalar', () => {

  const scalar = new DescriptionScalar();

  describe('#parseValue', () => {

    it('Should return a valid Description', () => {

      // Act
      const result = scalar.parseValue('dnsZZ');

      // Assert
      expect(result).toStrictEqual('dnsZZ');

    });

    it('Should throw an INVALID_DESCRIPTION error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('to$')).toThrow('INVALID_DESCRIPTION');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the Description', () => {

      // Act
      const result = scalar.serialize('dns1ZZ');

      // Assert
      expect(result).toStrictEqual('dns1ZZ');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid Name', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'dnsZZ',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('dnsZZ');

    });

    it('Should throw an INVALID_OBJECT_NAME error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'toto#',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.INT,
        value: '12',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow(
        'INVALID_DESCRIPTION',
      );
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_DESCRIPTION',
      );

    });

  });

});
