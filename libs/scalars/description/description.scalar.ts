import { Kind, ValueNode } from 'graphql';
import { ERROR, REGEX } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';


export class Description extends String {}

@Scalar('Description', /* istanbul ignore next */ () => Description)
export class DescriptionScalar implements CustomScalar<string, Description> {

  description = 'Description scalar type';

  private check(value: string): Description {
    const regExp = new RegExp(REGEX.DESCRIPTION);
    if (regExp.test(value)) {
      return value as Description;
    }
    throw new Error(ERROR.INVALID_DESCRIPTION);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): Description {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): Description {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_DESCRIPTION);
  }
}
