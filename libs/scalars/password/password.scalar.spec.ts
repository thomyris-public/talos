import { Kind, ValueNode } from 'graphql';

import { PasswordScalar } from './password.scalar';

describe('PasswordScalar', () => {

  const scalar = new PasswordScalar();

  describe('#parseValue', () => {

    it('Should return a valid Password', () => {

      // Act
      const result = scalar.parseValue('Pané123*');

      // Assert
      expect(result).toStrictEqual('Pané123*');

    });

    it('Should return a valid Password', () => {

      // Act
      const result = scalar.parseValue('Password24@');

      // Assert
      expect(result).toStrictEqual('Password24@');

    });

    it('Should throw an INVALID_PASSWORD error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('password24@')).toThrow(
        'INVALID_PASSWORD',
      );

    });

    it('Should throw an INVALID_PASSWORD error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('Password@')).toThrow('INVALID_PASSWORD');

    });

    it('Should throw an INVALID_PASSWORD error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('Password24')).toThrow('INVALID_PASSWORD');

    });

    it('Should throw an INVALID_PASSWORD error', () => {

      // Act and Assert
      expect(() => scalar.parseValue('to')).toThrow('INVALID_PASSWORD');

    });

  });

  describe('#serialize', () => {

    it('Should return a string representing the Password', () => {

      // Act
      const result = scalar.serialize('Password24@');

      // Assert
      expect(result).toStrictEqual('Password24@');

    });

  });

  describe('#parseLiteral', () => {

    it('Should return a valid Password', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'Password24@',
      };

      // Act
      const result = scalar.parseLiteral(tValueNode);

      // Assert
      expect(result).toStrictEqual('Password24@');

    });

    it('Should throw an INVALID_PASSWORD error', () => {

      // Arrange
      const tValueNode: ValueNode = {
        kind: Kind.STRING,
        value: 'toto#',
      };
      const tValueNode2: ValueNode = {
        kind: Kind.INT,
        value: '12',
      };

      // Act and Assert
      expect(() => scalar.parseLiteral(tValueNode)).toThrow('INVALID_PASSWORD');
      expect(() => scalar.parseLiteral(tValueNode2)).toThrow(
        'INVALID_PASSWORD',
      );

    });

  });

});
