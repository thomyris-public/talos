import { Kind, ValueNode } from 'graphql';
import { ERROR, REGEX } from 'talos-commons';
import { CustomScalar, Scalar } from '@nestjs/graphql';


export class Password extends String {}

@Scalar('Password', /* istanbul ignore next */ () => Password)
export class PasswordScalar implements CustomScalar<string, Password> {

  description = 'Password scalar type';

  private check(value: string): Password {
    const regex = new RegExp(REGEX.PASSWORD);

    if (regex.test(value)) {
      return value as Password;
    }
    throw new Error(ERROR.INVALID_PASSWORD);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parseValue(value: any): Password {
    return this.check(value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  serialize(value: any): string {
    return value as string;
  }

  parseLiteral(ast: ValueNode): Password {
    if (ast.kind === Kind.STRING) {
      return this.check(ast.value);
    }
    throw new Error(ERROR.INVALID_PASSWORD);
  }
}
